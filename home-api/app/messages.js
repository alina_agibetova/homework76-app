const express = require('express');
const router = express.Router();
const db = require('../fileDb')


const messages = [];
let messageId = 1;


router.get('/', (req, res) => {
  let messages = [];
  if (db.getItems().length > 30) {
      messages = db.getItems().slice(db.getItems().length - 30, db.getItems().length);
    } else {
      messages = db.getItems();

    }
    return res.send(messages);
});

router.get(`/messages?datetime=`, (req, res) => {
  const date = new Date(req.query.datetime);
  let messages = [];
  if (req.query.datetime && isNaN(date.getDate())) {
    return res.status(404).send({message: 'Date false'});
    } else {
      let index = db.getItems().find( message => message.datetime = req.query.datetime);
      messages = db.getItems().slice(index + 1, db.getItems().length);
      res.send(messages);
    }
    return res.send(messages);
})


router.post('/', async (req, res, next) => {
  try{
    if (!req.body.author || !req.body.message ){
      return res.status(404).send({message: 'Information incomplete'});
    }
    const message = {
      author: req.body.author,
      message: req.body.message,
      datetime: new Date(),
    };

    await db.addItem(message);

    return res.send({message: 'Create' + message.id})
  } catch (e){
    next(e);
  }
});

module.exports = router;