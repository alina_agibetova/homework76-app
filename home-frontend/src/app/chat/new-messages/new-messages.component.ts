import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessageData } from '../../../../models/message.model';
import { MessagesService } from '../../services/messages.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-messages',
  templateUrl: './new-messages.component.html',
  styleUrls: ['./new-messages.component.sass']
})
export class NewMessagesComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  constructor(private messageService: MessagesService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const value: MessageData = this.form.value;
    this.messageService.createMessage(value).subscribe(() => {
      console.log(value);
      void this.router.navigate(['/']);
    })
  }
}
