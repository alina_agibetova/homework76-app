import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message, MessageData } from '../../../models/message.model';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private http: HttpClient) { }

  getMessages(){
    return this.http.get<Message[]>('http://localhost:8000/messages').pipe(
      map(response => {
        return response.map(messageData => {
          return new Message(
            messageData.author,
            messageData.message,
            messageData.datetime,
            messageData.id,
          );
        });
      })
    )
  }

  datetime = '';

  getNewMessages(){
    return this.http.get<Message[]>(`http://localhost:8000/messages?datetime=${this.datetime}`).pipe(
      map(response => {
        return response.map(messageData => {
          return new Message(
            messageData.author,
            messageData.message,
            messageData.datetime,
            messageData.id,
          );
        });
      })
    )
  }

  createMessage(messageData: MessageData){
    return this.http.post('http://localhost:8000/messages', messageData);
  }
}
