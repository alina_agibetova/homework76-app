import { Component, OnInit } from '@angular/core';
import { Message } from '../../models/message.model';
import { MessagesService } from './services/messages.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{
  messages: Message[] = [];
  constructor(private messageService: MessagesService) {
  }

  ngOnInit(): void {
    let date = new Date();

    setInterval(() => {
      if (date){
        this.messageService.getMessages().subscribe(messages => {
          this.messages = messages.reverse();
        });
      } else {
        this.messageService.getNewMessages().subscribe(messages => {
          this.messages = messages.reverse();
        })
      }
    }, 4000)
  }
}
