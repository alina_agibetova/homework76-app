export  class Message {
  constructor(
    public author: string,
    public message: string,
    public datetime: string,
    public id: string,
  ) {}
}


export interface MessageData {
  author: string,
  message: string,
}
